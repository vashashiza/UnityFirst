﻿using System;


public class Source
{

}

public class PlayerArg: EventArgs
{
    public Player Player;

    public PlayerArg()
    {

    }

    public PlayerArg(Player player)
    {
        Player = player;
    }
}

public class DemageArg : EventArgs
{
    public Unit Agressor;

    public DemageArg()
    {

    }

    public DemageArg(Unit agressor)
    {
        Agressor = agressor;
    }
}