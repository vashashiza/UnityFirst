﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundManager : MonoBehaviour
{
    private static SoundManager instanse;
    public static SoundManager Instanse
    {
        get
        {
            return instanse;
        }
        set
        {
            if (instanse == null)
            {
                instanse = value;
            }
            else
            {
                Destroy(value);
            }
        }
    }

    public void Awake()
    {
        Instanse = this;
    }    

    public List<AudioSource> AudioSources = new List<AudioSource>();

    private List<AudioSource> instantiatedAudioSources = new List<AudioSource>();

    public void PlaySound(string clipName)
    {
        AudioSource audioSource = instantiatedAudioSources.Find(x => x.clip.name == clipName);
        
        if (audioSource == null)
        {
            audioSource= AudioSources.Find(x => x.clip.name == clipName);
            if (audioSource != null)
            {
                audioSource = Instantiate(audioSource);//подменяем на инстанс
                audioSource.transform.SetParent(transform);
                instantiatedAudioSources.Add(audioSource);
            }
        }
        else
        {
            audioSource.Play();
        }
    }
}
