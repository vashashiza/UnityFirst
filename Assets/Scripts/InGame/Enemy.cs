﻿using UnityEngine;
using System.Collections;
using System;

public class Enemy : Unit
{
    public Had Had;
    public float BeforeShot = 1;

    public float MinDistance = 1;

    protected override void move()
    {
        if (GameController.Instanse.PlayerUnit == null) return;

        if (Vector3.Distance(transform.position, GameController.Instanse.PlayerUnit.transform.position) >= MinDistance)
        {
            transform.LookAt(GameController.Instanse.PlayerUnit.transform);
            transform.position = Vector3.MoveTowards(transform.position, GameController.Instanse.PlayerUnit.transform.position, MoveSpeed * Time.deltaTime);
        }
    }

    protected override void Awake()
    {
        Had.OnPlayerEnter += Had_OnPlayer;
        Had.OnPlayerStay += Had_OnPlayer;
    }

    private IEnumerator startShot(Player player)
    {
        yield return new WaitForSeconds(BeforeShot);
        player.SetDamage(this, Damage);
        isReload = false;
    }



    private void Had_OnPlayer(object sender, EventArgs e)
    {
        if (isReload)
        {
            PlayerArg playerArg = e as PlayerArg;
            StartCoroutine(startShot(playerArg.Player));
        }       
    }
}
