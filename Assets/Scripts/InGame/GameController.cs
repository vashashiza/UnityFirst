﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class GameController : MonoBehaviour
{
    private static GameController instanse;
    public static GameController Instanse
    {
        get
        {
            return instanse;
        }
        private set
        {
            if (instanse == null)
            {
                instanse = value;
            }
            else if (value != instanse)
            {
                Destroy(value);
            }
        }
    }

    public void StartCountdown(float seconds)
    {
        StartCoroutine(countdown(seconds));
    }

    private IEnumerator countdown(float seconds)
    {
        ReloadText.color = new Color(1, 0, 0);

        while (seconds > 0)
        {
            ReloadText.text = Math.Round(seconds, 2) + "";
            yield return new WaitForEndOfFrame();
            seconds -= Time.deltaTime;
        }

        ReloadText.color = new Color(0, 1, 0);
        ReloadText.text = "Reloaded";
    }

    public Text ReloadText;

    public int MaxEnemys;

    public int Kills = 0;

    public int SpawnLimit = 50;  

    private int toSpawn = 0;
   
    public Text Win;

    public Text Lose;

    public Text Counter;

    public Player PlayerUnit;    

    public Enemy EnemyPrefab;

    public List<GameObject> SpawnPoints = new List<GameObject>();
    public List<Enemy> Enemys = new List<Enemy>(); 

    public void Awake()
    {
        Instanse = this;
        MaxEnemys = GamePreference.MaxEnemys > 0 ? GamePreference.MaxEnemys : 5;
    }


    private void randomSpawn()
    {
        Transform spawmTransform = SpawnPoints[UnityEngine.Random.Range(0, SpawnPoints.Count)].transform;

        Enemy enemy = Instantiate(EnemyPrefab, spawmTransform.position, spawmTransform.rotation) as Enemy;

        enemy.OnDisintegrate += Enemy_OnDisintegrate;

        Enemys.Add(enemy);
    }

    private void Enemy_OnDisintegrate(object sender, EventArgs e)
    {
        Enemys.Remove(sender as Enemy);
        Kills += 1;
        Counter.text = MaxEnemys + "/" + Kills;

        if (Kills >= MaxEnemys)
        {
            EndOfGame(Win);
            return;
        }

        if (Enemys.Count < SpawnLimit && toSpawn < MaxEnemys)
        {
            StartCoroutine(deferredSpawn(SpawnLimit - Enemys.Count));
        }    
    }
  
    IEnumerator deferredSpawn(int count)
    {
        toSpawn += count;
        while (count > 0)
        {
            count -= 1;
            yield return new WaitForSeconds(UnityEngine.Random.Range(1, 3f));
            randomSpawn();
        }      
    }

    void Start()
    {
        StartCoroutine(deferredSpawn(MaxEnemys));

        Counter.text = MaxEnemys + "/0";
        PlayerUnit.OnDisintegrate += PlayerUnit_OnDisintegrate;
    }

    public IEnumerator DiferredAction(float delay, Action action)
    {
        yield return new WaitForSeconds(delay);
        action();

    }

    public void EndOfGame(Text text)
    {
        text.gameObject.SetActive(true);
        StartCoroutine(DiferredAction(2, delegate ()
        {
            PauseMenuManager.Instance.PauseMenuRise();
        }));
    }
    private void PlayerUnit_OnDisintegrate(object sender, EventArgs e)
    {
        EndOfGame(Lose);
    }

    public void Update()
    {      
       
    }   

    private IEnumerator spawnRoutine(int count)
    {
        toSpawn += count;
        while (count > 0)
        {
            count -= 1;
            yield return new WaitForSeconds(UnityEngine.Random.Range(10, 13f));
            randomSpawn();
        }
    }



}

