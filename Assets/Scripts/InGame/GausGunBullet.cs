﻿using UnityEngine;
using System.Collections;

public class GausGunBullet : Bullet
{
    protected override void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            Enemy enemy = other.gameObject.GetComponentInChildren<Enemy>();
            enemy.SetDamage(this, Damage);
        }
    }
}
