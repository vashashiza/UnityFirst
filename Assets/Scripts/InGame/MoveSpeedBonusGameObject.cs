﻿using UnityEngine;
using System.Collections;

public class MoveSpeedBonusGameObject : BonusGameObject
{
    public override Bonus GetBonus
    {
        get
        {
            return BonusField;
        }
    }

    public MoveSpeedBonus BonusField = new MoveSpeedBonus();
}
