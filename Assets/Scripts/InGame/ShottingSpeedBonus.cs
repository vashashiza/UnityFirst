﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class ShottingSpeedBonus : Bonus
{
    public float ReloadClipFactor = 0;

    public float ReloadBulletFactor = 0;
}