﻿using UnityEngine;
using System.Collections;

public class Bullet : Unit
{
    public Vector3 Direction = new Vector3();   

    public float Range = 200;

    private Vector3 startPosition;

    protected override void move()
    {
        Vector3 moveSpeedVector3 = (Direction * MoveSpeed) * Time.deltaTime;
        transform.position += moveSpeedVector3;
        if (Vector3.Distance(transform.position, startPosition) >= Range)
        {
            Disintegrate();
        }
    }   

    protected override void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            Enemy enemy = other.gameObject.GetComponentInChildren<Enemy>();
            enemy.SetDamage(this, Damage);
            Disintegrate();
        }
    }

    protected override void Start()
    {
        startPosition = transform.position;
    }
}
