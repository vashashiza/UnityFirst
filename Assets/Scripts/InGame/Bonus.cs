﻿using UnityEngine;
using System;
using System.Collections;


[Serializable]
public class Bonus
{
    [SerializeField]
    private float lifeTime;
    public virtual float LifeTime
    {
        get
        {
            return lifeTime;
        }
        set
        {
            if (value <= 0)
            {
                lifeTime = 0;
            }
            else lifeTime = value;
        }
    }
}
