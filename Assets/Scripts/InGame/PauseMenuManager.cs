﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PauseMenuManager : MonoBehaviour
{
    private static PauseMenuManager instance;
    public static PauseMenuManager Instance
    {
        get
        {
            if (instance == null)
            {
                GameObject newGameObject = new GameObject("PauseMenuManager");
                newGameObject.AddComponent<PauseMenuManager>();
            }
            return instance;
        }
        private set
        {
            if (instance == null)
            {
                instance = value;
            }
            else
            {
                Destroy(value);
            }
        }
    }

    public void Awake()
    {
        Instance = this;
    }

    public GameObject PauseMenuPanel;

    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        Time.timeScale = 1;
    }

    public void ReturnToMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
        Time.timeScale = 1;
    }

    public void ResumeGame()
    {
        PauseMenuPanel.SetActive(false);
        Time.timeScale = 1;
    }

    public void PauseMenuRise()
    {
        PauseMenuPanel.SetActive(true);
        Time.timeScale = 0;
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (PauseMenuPanel.activeInHierarchy)
            {
                ResumeGame();
            }
            else
            {
                PauseMenuRise();
            }
            
        }
    }
}