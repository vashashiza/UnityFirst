﻿using UnityEngine;
using System.Collections;

public class ShieldBonusGameObject : BonusGameObject
{
    public override Bonus GetBonus
    {
        get
        {
            return BonusField;
        }
    }

    public ShieldBonus BonusField = new ShieldBonus();
}