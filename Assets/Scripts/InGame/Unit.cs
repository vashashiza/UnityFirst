﻿using UnityEngine;
using System.Collections;
using System;

public class Unit : MonoBehaviour
{
    private bool baseIsReload = true;
    public virtual bool isReload
    {
        get
        {
            return baseIsReload;
        }
        set
        {
            baseIsReload = value;
            if (!value)
            {
                StartCoroutine(reload(ReloadTime));
            }               
        }
    }

    protected float ReloadTime = 5;
    public virtual IEnumerator reload(float reloadTime)
    {
        yield return new WaitForSeconds(reloadTime);

        isReload = true;
    }
    public event EventHandler OnDisintegrate;
    protected virtual void riseOnDisintegrate()
    {
        if (OnDisintegrate != null) OnDisintegrate(this, new EventArgs());
    }

    public event EventHandler OnMurder;
    protected void riseOnMurder(Unit agressor)
    {
        if (OnMurder != null) OnMurder(this, new DemageArg(agressor));
    }

    private bool baseMoveActive = true;
    protected virtual bool moveActive
    {
        get
        {
            return baseMoveActive;
        }
        set
        {
            baseMoveActive = value;
        }
    }

    public float Damage = 10;

    [SerializeField]
    protected float baseHitPoints = 3;
    public virtual float HitPoints
    {
        get
        {
            return baseHitPoints;
        }
        protected set
        {
            if (value <= 0.000001f)
            {
                baseHitPoints = 0;
            }
            else
            {
                baseHitPoints = value;
            }
        }
    }

    public float MoveSpeed
    {
        get
        {
            return BaseMoveSpeed + BonusMoveSpeed;
        }
    }

    public float BonusMoveSpeed = 0;

    public float BaseMoveSpeed = 10;

    public virtual void SetDamage(Unit sender, float damage)
    {
        if (HitPoints == 0) return;

        HitPoints -= damage;
        if (HitPoints <= 0)
        {
            riseOnMurder(sender);
            Disintegrate();
        }
    }

    public virtual void Disintegrate()
    {
        if (this != null)
        {
            riseOnDisintegrate();
            Destroy(gameObject);
        }
    }

    protected virtual void Start()
    {

    }

    protected virtual void Update()
    {
        if (moveActive) move();
    }

    protected virtual void OnCollisionStay(Collision collision)
    {

    }

    protected virtual void OnCollisionExit(Collision collision)
    {

    }

    protected virtual void OnCollisionEnter(Collision collision)
    {

    }

    protected virtual void OnTriggerStay(Collider other)
    {

    }

    protected virtual void OnTriggerExit(Collider other)
    {

    }

    protected virtual void OnTriggerEnter(Collider other)
    {

    }

    protected virtual void move()
    {

    }

    protected virtual void Awake()
    {

    }
}
