﻿using UnityEngine;
using System.Collections;
using System;

public class Player : Unit
{
    public Gun[] Guns = new Gun[10];

    public Gun StartGunPrefab;

    public GameObject GunPoint;

    public float ReloadClipFactor = 1;

    public float ReloadBulletFactor = 1;

    private Gun nowGunKeeper;
    public Gun nowGun
    {
        get
        {
            return nowGunKeeper;
        }
        set
        {
            if (nowGunKeeper != null) nowGunKeeper.gameObject.SetActive(false);
            nowGunKeeper = value;

            value.gameObject.SetActive(true);
        }
    }

    public bool Immortal = false;

    public override float HitPoints
    {
        get
        {
            return base.HitPoints;
        }

        protected set
        {
            if (!Immortal || value > base.HitPoints)
            {
                base.HitPoints = value;
            }
        }
    }

    protected virtual void setNewGun(Gun prefab)
    {
        nowGun = Instantiate(prefab, GunPoint.transform.position, GunPoint.transform.rotation) as Gun;
        nowGun.transform.SetParent(GunPoint.transform);

        nowGun.OvnerPlayer = this;

        Guns[nowGun.BindPoint] = nowGun;
    }

    protected override void Awake()
    {
        setNewGun(StartGunPrefab);
    }      

    protected override void Update()
    {
        base.Update();
        if (Input.GetMouseButton(0))
        {            
            nowGun.Shot();
        }
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 100))
        {
            Vector3 lookPoint = hit.point;
            lookPoint.y = 1;
            transform.LookAt(lookPoint);
        }       
    }
    
    protected override void OnTriggerEnter(Collider other)
    {
        if (other.tag == "GunBonus")
        {
            GunBonus gunBonus = other.gameObject.GetComponent<GunBonus>();
            setNewGun(gunBonus.GunPrefab);

            gunBonus.Disintegrate();
        }
        else if (other.tag == "Bonus")
        {
            BonusGameObject bonusGameObject = other.gameObject.GetComponent<BonusGameObject>();

            if (bonusGameObject.GetBonus is MoveSpeedBonus) useBonus(bonusGameObject.GetBonus as MoveSpeedBonus);
            if (bonusGameObject.GetBonus is ShieldBonus) useBonus(bonusGameObject.GetBonus as ShieldBonus);
            if (bonusGameObject.GetBonus is ShottingSpeedBonus) useBonus(bonusGameObject.GetBonus as ShottingSpeedBonus);

            bonusGameObject.Disintegrate();
        }
    }

    private void useBonus(MoveSpeedBonus bonus)
    {
        BonusMoveSpeed += bonus.MoveSpeedValue;
        StartCoroutine(differedAction(bonus.LifeTime, delegate ()
        {
            BonusMoveSpeed -= bonus.MoveSpeedValue;
        }));
    }

    private void useBonus(ShieldBonus bonus)
    {
        Immortal = true;
        StartCoroutine(differedAction(bonus.LifeTime, delegate ()
        {
            Immortal = false;
        }));
    }

    private void useBonus(ShottingSpeedBonus bonus)
    {
        ReloadBulletFactor -= bonus.ReloadBulletFactor;
        ReloadClipFactor -= bonus.ReloadClipFactor;
        StartCoroutine(differedAction(bonus.LifeTime, delegate ()
        {
            ReloadBulletFactor += bonus.ReloadBulletFactor;
            ReloadClipFactor += bonus.ReloadClipFactor;
        }));
    }

    private IEnumerator differedAction(float delay, Action call)
    {
        yield return new WaitForSeconds(delay);
        call();
    }

    private void setGun(int position)
    {
        if (Guns[position] != null)
        {
            nowGun = Guns[position];
        }
    }

    protected override void move()
    {
        Vector3 direction = new Vector3();

        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
        {
            direction += new Vector3(0, 0, 1);
        }

        if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
        {
            direction += new Vector3(0, 0, -1);
        }

        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
        {
            direction += new Vector3(-1, 0, 0);
        }

        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            direction += new Vector3(1, 0, 0);
        }
        if (Input.GetKey(KeyCode.R))
        {
            nowGun.ReloadingClip();
        }        

        transform.position += (direction.normalized * MoveSpeed) * Time.deltaTime;

        if (Input.GetKey(KeyCode.Alpha0))
        {
            setGun(0);
        }

        if (Input.GetKey(KeyCode.Alpha1))
        {
            setGun(1);
        }

        if (Input.GetKey(KeyCode.Alpha2))
        {
            setGun(2);
        }

        if (Input.GetKey(KeyCode.Alpha3))
        {
            setGun(3);
        }

        if (Input.GetKey(KeyCode.Alpha4))
        {
            setGun(4);
        }

        if (Input.GetKey(KeyCode.Alpha5))
        {
            setGun(5);
        }

        if (Input.GetKey(KeyCode.Alpha6))
        {
            setGun(6);
        }

        if (Input.GetKey(KeyCode.Alpha7))
        {
            setGun(7);
        }

        if (Input.GetKey(KeyCode.Alpha8))
        {
            setGun(8);
        }

        if (Input.GetKey(KeyCode.Alpha9))
        {
            setGun(9);
        }
    }
}
