﻿using UnityEngine;
using System.Collections;

public class BonusGameObject : MonoBehaviour
{
    public virtual Bonus GetBonus { get; private set; }

    public virtual void Disintegrate()
    {
        if (this != null)
        {
            Destroy(gameObject);
        }
    }
}
