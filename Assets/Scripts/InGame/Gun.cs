﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Gun : MonoBehaviour
{
    public float BaseReloadBulletTime = 1;

    private float BonusReloadBulletFactor
    {
        get
        {
            return (OvnerPlayer != null ? OvnerPlayer.ReloadBulletFactor : 1);
        }
    }

    public float BaseReloadClipTime = 3;

    public float BonusReloadClipFactor
    {
        get
        {
            return (OvnerPlayer != null ? OvnerPlayer.ReloadClipFactor : 1);
        }
    }

    public float ReloadBulletTime
    {
        get
        {
            return BaseReloadBulletTime * BonusReloadBulletFactor;
        }
    }

    public float ReloadClipTime
    {
        get
        {
            return BaseReloadClipTime * BonusReloadClipFactor;
        }
    }


    public int ClipSizeMax = 10;

    private bool baseIsReload = true;
    public virtual bool IsReload
    {
        get
        {
            return baseIsReload;
        }
        set
        {
            baseIsReload = value;
            if (!value)
            {
                StartCoroutine(reload());
            }   
        }
    }

    public Player OvnerPlayer;

    public virtual IEnumerator reload()
    {
        yield return new WaitForSeconds(ReloadBulletTime);

        IsReload = true;
    }
    public Bullet BulletPrefab;    

    public string ShotSoundName = "";
    public int BindPoint = 0;

    private int clipSizeNow = 0;
    public int ClipSizeNow
    {
        get
        {
            return clipSizeNow;
        }
        set
        {
            if (value <= 0)
            {
                clipSizeNow = 0;
                StartCoroutine(reloadClip());
            }
            else
            {
                clipSizeNow = value;
            }
        }
    }

    protected virtual void ShotSoundPlay()
    {
        SoundManager.Instanse.PlaySound(ShotSoundName);
    }

    protected virtual bool shotTarget(out Vector3 shotTarget)
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        Vector3 toReturn = new Vector3();
        if (Physics.Raycast(ray, out hit, 100))
        {
            toReturn = hit.point - transform.position;
            toReturn.y = 0;
            shotTarget = toReturn.normalized;
            return true;
        }
        shotTarget = toReturn;
        return false;
    }

    protected virtual Vector3? shotTarget()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 100))
        {
            Vector3 toReturn = hit.point - transform.position;
            toReturn.y = 0;

            return toReturn.normalized;
        }
        return null;
    }

    public virtual IEnumerator reloadClip()
    {
        GameController.Instanse.StartCountdown(ReloadClipTime);
        yield return new WaitForSeconds(ReloadClipTime);

        ClipSizeNow = ClipSizeMax;
    }

    protected virtual void spawnBullet(Vector3 direction)
    {
        Bullet bullet = Instantiate(BulletPrefab, transform.position, transform.rotation) as Bullet;
        bullet.Direction = direction;
    }

    public virtual void Shot()
    {
        /*
        Vector3 target;
        if (shotTarget(out target))
        {
            spawnBullet(target);
        }
        */

        Vector3? target = shotTarget();
        if (target != null && ClipSizeNow > 0 && IsReload)
        {
            spawnBullet((Vector3)target);
            ShotSoundPlay();

            IsReload = false;
            ClipSizeNow -= 1;           
        }
    }

    public void ReloadingClip()
    {
        ClipSizeNow = 0;        
    }

    public void Start()
    {
        ClipSizeNow = ClipSizeMax;
    }
}
