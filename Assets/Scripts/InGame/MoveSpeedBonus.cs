﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class MoveSpeedBonus : Bonus
{
    public float MoveSpeedValue = 0;
}
