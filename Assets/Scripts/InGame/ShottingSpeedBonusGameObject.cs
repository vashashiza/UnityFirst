﻿using UnityEngine;
using System.Collections;

public class ShottingSpeedBonusGameObject : BonusGameObject
{
    public override Bonus GetBonus
    {
        get
        {
            return BonusField;
        }
    }

    public ShottingSpeedBonus BonusField = new ShottingSpeedBonus();
}
