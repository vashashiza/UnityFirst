﻿using UnityEngine;
using System.Collections;
using System;

public class Had : MonoBehaviour
{
    public event EventHandler OnPlayerEnter;

    public event EventHandler OnPlayerStay;

    protected virtual void Awake()
    {

    }

    protected virtual void Start()
    {

    }

    protected virtual void Update()
    {

    }

    protected virtual void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            Player player = other.GetComponent<Player>();

            if (OnPlayerStay != null) OnPlayerStay(this, new PlayerArg(player));
        }
    }

    protected virtual void OnTriggerExit(Collider other)
    {

    }

    protected virtual void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Player player = other.GetComponent<Player>();

            if (OnPlayerEnter != null) OnPlayerEnter(this, new PlayerArg(player));
        }
    }
}
