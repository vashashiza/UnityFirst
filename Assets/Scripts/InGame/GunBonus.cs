﻿using UnityEngine;
using System.Collections;

public class GunBonus : MonoBehaviour
{
    public Gun GunPrefab;

    public void Disintegrate()
    {
        Destroy(gameObject);
    }
}
