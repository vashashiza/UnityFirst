﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Rocket : Bullet
{
    public SphereCollider ThisCollider;

    public GameObject BurstVisual;

    private List<Enemy> enemys = new List<Enemy>();

    public float BurstRadius = 5;

    public float DeferredDisintegrateTime = 2;

    private IEnumerator burst()
    {
        moveActive = false;
        ThisCollider.radius = BurstRadius;
        BurstVisual.SetActive(true);

        yield return new WaitForFixedUpdate();
        yield return new WaitForFixedUpdate();

        foreach (Enemy item in enemys)
        {
            item.SetDamage(this, Damage);
        }

        Disintegrate();
    }

    public override void Disintegrate()
    {
        StartCoroutine(deferredDisintegrate());
    }

    private IEnumerator deferredDisintegrate()
    {
        ThisCollider.enabled = false;
        yield return new WaitForSeconds(DeferredDisintegrateTime);
        base.Disintegrate();
    }

    protected override void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            Enemy enemy = other.gameObject.GetComponent<Enemy>();

            if (enemys.Count == 0) StartCoroutine(burst());

            enemys.Add(enemy);
        }
    }
}
