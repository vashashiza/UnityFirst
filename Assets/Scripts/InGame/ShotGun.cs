﻿using UnityEngine;
using System.Collections;


public class ShotGun : Gun
{
    public int BulletsCount = 8;

    public override void Shot()
    {
        if (IsReload)
        {
            IsReload = false;
            Vector3 target;

            if (shotTarget(out target))
            {
                float angle = transform.parent.rotation.eulerAngles.y * Mathf.Deg2Rad;
                float scatter = 3 * Mathf.Deg2Rad;

                spawnBullet(target);

                float rightAngle = angle;
                for (int i = 1; i < BulletsCount / 2; i++)
                {
                    rightAngle += scatter;
                    float x = Mathf.Cos(rightAngle) - Mathf.Sin(rightAngle);
                    float z = Mathf.Cos(rightAngle) + Mathf.Sin(rightAngle);
                    Vector3 direction = target +(new Vector3(x, 0, z));
                    spawnBullet(direction);
                }

                float leftAngle = angle;
                for (int i = 1; i < BulletsCount / 2; i++)
                {
                    leftAngle -= scatter;
                    float x = Mathf.Cos(leftAngle) - Mathf.Sin(leftAngle);
                    float z = Mathf.Cos(leftAngle) + Mathf.Sin(leftAngle);
                    Vector3 direction = target + (new Vector3(x, 0, z));
                    spawnBullet(direction);

                }
            }

        }
    }
}