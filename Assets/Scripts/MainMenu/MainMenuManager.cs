﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.Events;
using System;
using UnityEngine.UI;
using System.Collections.Generic;

public class MainMenuManager : MonoBehaviour
{
    public void Awake()
    {
        EnemysCounter.onValueChanged.AddListener(EnemysCounter_OnValueChanged);
        EnemysCounter.onEndEdit.AddListener(EnemysCounter_OnEndEdit);
    }

    public void LoadGame()
    {
        SceneManager.LoadScene("1");
    }

    public InputField EnemysCounter;

    public List<char> conditionList = new List<char>();

    private string enemysCounterText = "";
    public void EnemysCounter_OnValueChanged(string text)
    {
        foreach (char item in text)
        {
            if (!conditionList.Contains(item))
            {
                EnemysCounter.text = enemysCounterText;
                return;
            }
        }
        enemysCounterText = EnemysCounter.text;
    }

    public void EnemysCounter_OnEndEdit(string text)
    {
        GamePreference.MaxEnemys = Convert.ToInt32(enemysCounterText);
    }
}
